﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirst
{
    class SampleContext: DbContext
    {
        public SampleContext() : base("MyDb")
        { }

        public DbSet<Genre> Genres { get; set; }
        public DbSet<Group> Groups { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Genre>()
                .HasKey(p => p.GenreId)
                .HasMany(t => t.Groupses);
               
        }
    }
}
