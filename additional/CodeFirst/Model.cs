﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace CodeFirst
{
    public class Genre
    {
        public int GenreId { get; set; }
        public string NameGenre { get; set; }
        public Genre()
        {
            this.Groupses = new HashSet<Group>();
        }

        public virtual ICollection<Group> Groupses { get; set; }
    } 
    public class Group
    {
        public int GroupId { get; set; }
        public string NameGroup { get; set; }
        public int GenreId { get; set; }
        public virtual Genre Genre { get; set; }





    }
}
